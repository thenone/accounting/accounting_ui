import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { SerializerService } from '../../../shared/services/serializer/serializer.service';

import { Payment } from '../../../shared/models/payment';

import { environment } from '../../../../environments/environment';

@Injectable()
export class PaymentsService {

  constructor(private http: HttpClient, private serializer: SerializerService) { }

  register(payment: Payment) {
    return this.http.post(environment.baseUrl + '/admin/payment', this.serializer.serialize(payment));
  }

  addToMembershipFee(payload: MembershipPayload) {
    return this.http.post(environment.baseUrl + '/admin/membership_fee', this.serializer.serialize(payload));
  }

}

class MembershipPayload {
  total: number;
  customer_id: string;
  date: Date;
}
