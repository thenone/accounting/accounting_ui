import { Injectable } from '@angular/core';

import * as PersianDate from 'persian-date';

@Injectable()
export class PersianDateService {

  constructor() { }

  Gregorian = 'gregorian';
  Jalali = 'jalali';

  private standardDate: number[];

  static toPersian(date: string, format = 'YYYY M D') {
    return new PersianDate(new Date(date)).format(format);
  }

  private normalize(date: string, delimiter: string): number[] {
    this.standardDate = [];
    for (const item of date.split(delimiter)) {
      this.standardDate.push(parseInt(item, 10));
    }
    return this.standardDate;
  }

  convert(date: string, delimiter: string, calendar: string = this.Gregorian) {
    this.normalize(date, delimiter);
    return new Date(new PersianDate(this.standardDate).toCalendar(calendar).ON.gDate);
  }

  ISODate(date: string, delimiter: string, calendar: string = this.Gregorian) {
    this.normalize(date, delimiter);
    return (<Date>new PersianDate(this.standardDate).toCalendar(calendar).ON.gDate).toISOString();
  }
}
