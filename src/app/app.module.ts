import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

// Modules
import { AppRoutingModule } from './app-routing.module';
import { ShellModule } from './shell/shell.module';
import { SharedModule } from './shared/shared.module';

// Services
import { AuthService } from './shared/services/auth/auth.service';
import { SerializerService } from './shared/services/serializer/serializer.service';
import { NotificationService } from './shared/services/notification/notification.service';
import { PersianDateService } from './shared/services/date/persian-date.service';

// Guards
import { AdminAreaGuard } from './shared/services/auth/guards/admin-area.guard';
import { AppAreaGuard } from './shared/services/auth/guards/app-area.guard';
import { LoginGuard } from './shared/services/auth/guards/login.guard';

// Components
import { AppComponent } from './app.component';
import { CustomerHomepageComponent } from './customer/customer-homepage/customer-homepage.component';

@NgModule({
  declarations: [
    AppComponent,
    CustomerHomepageComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ShellModule,
    SharedModule,
    AppRoutingModule
  ],
  providers: [
    AuthService,
    PersianDateService,
    SerializerService,
    NotificationService,
    AdminAreaGuard,
    AppAreaGuard,
    LoginGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
