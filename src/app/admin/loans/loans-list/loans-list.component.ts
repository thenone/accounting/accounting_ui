import { Component, OnInit } from '@angular/core';

import { NotificationService } from '../../../shared/services/notification/notification.service';
import { LoansService } from '../../services/loans/loans.service';

import { Loan } from '../../../shared/models/loan';

@Component({
  selector: 'app-loans-list',
  templateUrl: './loans-list.component.html',
  styleUrls: ['./loans-list.component.css']
})
export class LoansListComponent implements OnInit {

  constructor(private loansService: LoansService, private notify: NotificationService) { }

  loans: Loan[];

  ngOnInit() {
    this.loansService.getList().subscribe(
      loans => {
        this.loans = loans as Loan[];
      },
      error => {
        this.notify.error('خطا در دریافت لیست وام‌ها', 'دوباره امتحان کنید');
      }
    );
  }

}
