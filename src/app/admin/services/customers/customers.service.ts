import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { SerializerService } from '../../../shared/services/serializer/serializer.service';

import { environment } from '../../../../environments/environment';

@Injectable()
export class CustomersService {

  constructor(private http: HttpClient, private serializer: SerializerService) { }

  register(payload) {
    return this.http.post(environment.baseUrl + '/admin/customer', this.serializer.serialize(payload));
  }

  getIds(params?) {
    return this.http.get(environment.baseUrl + '/admin/customers', { params: params });
  }

  getList(params?) {
    return this.http.get(environment.baseUrl + '/admin/customers', { params: params });
  }
}
