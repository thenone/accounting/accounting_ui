import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

import { AuthService } from '../../../shared/services/auth/auth.service';
import { ValidatorService } from '../../../shared/services/validators/validator.service';
import { NotificationService } from '../../../shared/services/notification/notification.service';

import { AuthToken } from '../../../shared/models/auth-token';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  constructor(
    private auth: AuthService,
    private formBuilder: FormBuilder,
    private notify: NotificationService,
    private router: Router
  ) {
    this.createForm();
  }

  loginForm: FormGroup;

  private createForm() {
    this.loginForm = this.formBuilder.group({
      username: ['09', [Validators.required, ValidatorService.mobile]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  public onSubmit() {
    this.auth.login(this.loginForm.value).subscribe(
      res => this.onSuccess(res as AuthToken),
      error => this.onError(error)
    );
  }

  private onSuccess(res: AuthToken) {
    this.notify.success('خوش آمدید');
    this.auth.setToken(res.token);
    this.auth.setUserId(res.id);
    this.auth.setTokenExp(res.exp);
    if (res.is_admin) {
      this.auth.setIsAdmin(true);
      this.router.navigate(['/admin']).then(() => { console.log('Welcome Admin!'); });
      return;
    }
    this.router.navigate(['/']).then(() => { console.log('Welcome!'); });
  }

  private onError(error: HttpErrorResponse) {
    if (error.status === 401) {
      this.notify.error('نام کاربری یا رمز عبور اشتباه است');
    } else if (error.status === 400) {
      this.notify.error('خطا', error.error.message);
    }
  }
}
