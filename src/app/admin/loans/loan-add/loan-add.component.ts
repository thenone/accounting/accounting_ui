import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

import { LoansService } from '../../services/loans/loans.service';
import { ValidatorService } from '../../../shared/services/validators/validator.service';
import { CustomersService } from '../../services/customers/customers.service';
import { NotificationService } from '../../../shared/services/notification/notification.service';

import { Loan } from '../../../shared/models/loan';

@Component({
  selector: 'app-loan-add',
  templateUrl: './loan-add.component.html',
  styleUrls: ['./loan-add.component.css']
})
export class LoanAddComponent implements OnInit {

  constructor(
    private customersService: CustomersService,
    private notify: NotificationService,
    private loansService: LoansService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.createForm();
  }

  loanAddForm: FormGroup;
  customers: any;

  ngOnInit(): void {
    this.customersService.getIds({serialize_group: 'dropdown'}).subscribe(
      (customers) => {
        this.customers = customers;
      },
      error => {
        this.notify.error('خطا در دریافت لیست مشتریان', 'دوباره امتحان کنید');
      }
    );
  }

  private createForm() {
    this.loanAddForm = new FormGroup({
      total_price: new FormControl('', {
        validators: [Validators.required, ValidatorService.number]
      }),
      installments_qty: new FormControl(24, {
        validators: [Validators.required, ValidatorService.number],
        updateOn: 'blur'
      }),
      date: new FormControl(''),
      cheque_id: new FormControl('', {
        validators: [Validators.required, Validators.minLength(6), ValidatorService.number],
        updateOn: 'blur'
      }),
      customer_id: new FormControl('', {
        validators: [ValidatorService.customerChoice(this.customers)]
      })
    });
  }

  onSubmit() {
    this.loanAddForm.disable();
    this.loansService.register(this.loanAddForm.value).subscribe(
      res => this.onSuccess(res as Loan),
      err => this.onError(err),
      () => {
        this.loanAddForm.enable();
      }
    );
  }

  private onSuccess(res: Loan) {
    this.notify.success('با موفقیت ثبت شد');
    this.router.navigate(['../loans'], { relativeTo: this.activatedRoute });
  }

  private onError(error: HttpErrorResponse) {
    if (error.status === 400) {
      this.notify.error('خطا', error.error.message);
    }
  }
}
