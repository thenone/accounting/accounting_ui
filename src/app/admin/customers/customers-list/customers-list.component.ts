import { Component, OnInit } from '@angular/core';

import { CustomersService } from '../../services/customers/customers.service';
import { NotificationService } from '../../../shared/services/notification/notification.service';

import { Customer } from '../../../shared/models/customer';

@Component({
  selector: 'app-customers-list',
  templateUrl: './customers-list.component.html',
  styleUrls: ['./customers-list.component.css']
})
export class CustomersListComponent implements OnInit {

  constructor(private customersService: CustomersService, private notify: NotificationService) { }

  customers: Customer[];

  ngOnInit(): void {
    this.customersService.getList().subscribe(
      customers => {
        this.customers = customers as Customer[];
      },
      error => {
        this.notify.error('خطا در دریافت لیست مشتریان', 'دوباره امتحان کنید');
      }
    );
  }
}
