import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { CustomerAddComponent } from './customers/customer-add/customer-add.component';
import { CustomersListComponent } from './customers/customers-list/customers-list.component';
import { LoansListComponent } from './loans/loans-list/loans-list.component';
import { LoanAddComponent } from './loans/loan-add/loan-add.component';
import { PaymentAddComponent } from './payments/payment-add/payment-add.component';

const routes: Routes = [
  { path: '', component: AdminHeaderComponent, outlet: 'header' },
  { path: 'customer-add', component: CustomerAddComponent },
  { path: 'customers', component: CustomersListComponent },
  { path: 'loans', component: LoansListComponent },
  { path: 'loan-add', component: LoanAddComponent },
  { path: 'payment-add', component: PaymentAddComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
