import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

// Declarations
import { TitleComponent } from './components/title/title.component';
import { PersianDatePipe } from './pipes/persian-date.pipe';

// Services
import { AuthService } from './services/auth/auth.service';
import { PersianDateService } from './services/date/persian-date.service';
import { NotificationService } from './services/notification/notification.service';
import { SerializerService } from './services/serializer/serializer.service';
import { TokenInterceptorService } from './services/token-interceptor/token-interceptor.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    AuthService,
    PersianDateService,
    NotificationService,
    SerializerService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ],
  declarations: [
    TitleComponent,
    PersianDatePipe
  ],
  exports: [
    TitleComponent,
    PersianDatePipe
  ]
})
export class SharedModule { }
