import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Modules
import { AdminRoutingModule } from './admin-routing.module';
import { DpDatePickerModule } from 'ng2-jalali-date-picker';
import { SharedModule } from '../shared/shared.module';

// Declarations
import { AdminHomepageComponent } from './admin-homepage/admin-homepage.component';
import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { CustomerAddComponent } from './customers/customer-add/customer-add.component';
import { CustomersListComponent } from './customers/customers-list/customers-list.component';
import { LoansListComponent } from './loans/loans-list/loans-list.component';
import { LoanAddComponent } from './loans/loan-add/loan-add.component';
import { PaymentAddComponent } from './payments/payment-add/payment-add.component';

// Services
import { CustomersService } from './services/customers/customers.service';
import { LoansService } from './services/loans/loans.service';
import { PaymentsService } from './services/payments/payments.service';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DpDatePickerModule,
    SharedModule
  ],
  declarations: [
    AdminHomepageComponent,
    AdminHeaderComponent,
    CustomerAddComponent,
    CustomersListComponent,
    LoansListComponent,
    LoanAddComponent,
    PaymentAddComponent
  ],
  providers: [
    CustomersService,
    LoansService,
    PaymentsService
  ]
})
export class AdminModule { }
