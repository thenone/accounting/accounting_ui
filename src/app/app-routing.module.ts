import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Guards
import { AppAreaGuard } from './shared/services/auth/guards/app-area.guard';
import { AdminAreaGuard } from './shared/services/auth/guards/admin-area.guard';
import { LoginGuard } from './shared/services/auth/guards/login.guard';

// Components
import { LoginComponent } from './shell/pages/login/login.component';
import { NotFoundComponent } from './shell/pages/not-found/not-found.component';
import { CustomerHomepageComponent } from './customer/customer-homepage/customer-homepage.component';

const routes: Routes = [
  { path: '', component: CustomerHomepageComponent, canActivate: [AppAreaGuard], pathMatch: 'full' },
  { path: 'admin', loadChildren: 'app/admin/admin.module#AdminModule', canLoad: [AdminAreaGuard] },
  { path: 'login', component: LoginComponent, canActivate: [LoginGuard] },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
