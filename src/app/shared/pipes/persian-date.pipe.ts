import { Pipe, PipeTransform } from '@angular/core';

import { PersianDateService } from '../services/date/persian-date.service';

@Pipe({
  name: 'persianDate'
})
export class PersianDatePipe implements PipeTransform {

  transform(ISODate: string, format = 'l'): any {
    return PersianDateService.toPersian(ISODate, format);
  }

}
