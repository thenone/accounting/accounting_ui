import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

import { ValidatorService } from '../../../shared/services/validators/validator.service';
import { CustomersService } from '../../services/customers/customers.service';
import { NotificationService } from '../../../shared/services/notification/notification.service';
import { Customer } from '../../../shared/models/customer';

@Component({
  selector: 'app-customer-add',
  templateUrl: './customer-add.component.html',
  styleUrls: ['./customer-add.component.css']
})
export class CustomerAddComponent {

  constructor(
    private fb: FormBuilder,
    private customersService: CustomersService,
    private notify: NotificationService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.createForm();
  }

  formName = 'ثبت مشتری';
  customerAddForm: FormGroup;

  private createForm() {
    this.customerAddForm = this.fb.group({
      name: new FormGroup({
        first: new FormControl('', [Validators.required, ValidatorService.text]),
        last: new FormControl('', [Validators.required, ValidatorService.text])
      }),
      father_name: new FormControl('', [ValidatorService.text]),
      stocks: new FormControl('', [Validators.required]),
      initial_installments_amount: new FormControl('', [Validators.required, ValidatorService.number]),
      initial_membership_fee: new FormControl('', [Validators.required, ValidatorService.number]),
      mobile: new FormControl('09', {
        validators: [Validators.required, ValidatorService.mobile]
      }),
      national_id: new FormControl('06', {
        validators: [Validators.required, ValidatorService.nationalId]
      }),
      join_date: new FormControl('', {
        validators: [Validators.required]
      }),
      password: new FormGroup({
        first: new FormControl('', {
          validators: [Validators.required, Validators.minLength(6)]
        }),
        confirm: new FormControl('', {
          validators: [Validators.required]
        })
      }, {
        validators: [ValidatorService.passwordMatch]
      })
    });
  }

  public onSubmit() {
    this.customerAddForm.disable();
    this.customersService.register(this.customerAddForm.value).subscribe(
      res => this.onSuccess(res as Customer),
      err => this.onError(err),
      () => {
        this.customerAddForm.enable();
      }
    );
  }

  private onSuccess(res: Customer) {
    this.notify.success('با موفقیت ثبت شد', res.name.first + ' ' + res.name.last);
    this.router.navigate(['../customers'], { relativeTo: this.activatedRoute });
  }

  private onError(error: HttpErrorResponse) {
    if (error.status === 400) {
      this.notify.error('خطا', error.error.message);
    }
  }

}
