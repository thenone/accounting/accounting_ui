import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { SerializerService } from '../../../shared/services/serializer/serializer.service';

import { Loan } from '../../../shared/models/loan';
import { environment } from '../../../../environments/environment';

@Injectable()
export class LoansService {

  constructor(private http: HttpClient, private serializer: SerializerService) { }

  register(payload: Loan) {
    return this.http.post(environment.baseUrl + '/admin/loan', this.serializer.serialize(payload));
  }

  getList(params?) {
    return this.http.get(environment.baseUrl + '/admin/loans', { params: params });
  }
}
