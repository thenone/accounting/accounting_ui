export class Customer {
  name: {
    first: string,
    last: string
  };
  _id: string;
  fatherName?: string;
  stocks: number;
  joinDate: Date;
  mobile: number;
  nationalId: number;
}
