export class AuthToken {
  id: string;
  token: string;
  exp: Date;
  is_admin?: boolean;
}
