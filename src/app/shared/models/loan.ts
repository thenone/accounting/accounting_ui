import { Customer } from './customer';

export class Loan {
  _id: string;
  totalPrice: number;
  installmentsQty: number;
  payments: {
    normal: {
      amount: number;
      qty: number;
      paidQty: number;
    },
    final: {
      amount: number;
      paid: boolean;
    }
  };
  date: Date;
  chequeId: number;
  customer: Customer;
}
