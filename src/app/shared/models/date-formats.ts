export class DateFormats {
  year: {
    fullYear: 'YYYY';
    halfYear: 'YY';
  };
  month: {
    normal: 'M',
    zeroLeaded: 'MM',
    named: 'MMMM'
  };
  day: {
    normal: 'D',
    zeroLeaded: 'DD',
    weekday: 'ddd'
  };
  hour: {
    normal: 'H',
    zeroLeaded: 'HH'
  };
  minute: {
    normal: 'm',
    zeroLeaded: 'mm'
  };
  second: {
    normal: 's',
    zeroLeaded: 'ss'
  };
}
