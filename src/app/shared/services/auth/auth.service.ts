import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { SerializerService } from '../serializer/serializer.service';
import { environment } from '../../../../environments/environment';


@Injectable()
export class AuthService {
  constructor(private http: HttpClient, private serializer: SerializerService) { }

  private admin = false;
  private tokenExp: Date;
  private userId: string;

  login(payloads) {
    return this.http.post(environment.baseUrl + '/login', this.serializer.serialize(payloads));
  }

  logout() {
    localStorage.removeItem('accounting_token');
    this.userId = undefined;
    this.userId = undefined;
    this.tokenExp = undefined;
    this.admin = false;
  }

  isLoggedIn(): boolean {
    return (this.tokenExp > new Date && !!this.getToken() && !!this.getUserId());
  }

  setUserId(id: string) {
    this.userId = id;
  }

  getUserId(): string | undefined {
    return this.userId;
  }

  setTokenExp(expDate: Date): void {
    this.tokenExp = new Date(expDate);
  }

  getTokenExp(): Date {
    return this.tokenExp;
  }

  setIsAdmin(isAdmin: boolean): void {
    this.admin = isAdmin;
  }

  isAdmin(): boolean {
    return this.admin;
  }

  setToken(token: string): void {
    localStorage.setItem('accounting_token', token);
  }

  getToken(): string {
    return localStorage.getItem('accounting_token');
  }
}
