export class Payment {
  constructor() {
    this.installment = {
      type: 'normal',
      amount: 0
    };
    this.installment_type = 'normal';
  }
  total: number;
  loan: string;
  installment: {
    type: string;
    amount: number;
  };
  installment_type: string;
  date: Date;
  docId: number;
  membershipFee: number;
  extra: string;
}
