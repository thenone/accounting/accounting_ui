import { Injectable } from '@angular/core';
import { CanLoad, Route } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { AuthService } from '../auth.service';

@Injectable()
export class AdminAreaGuard implements CanLoad {

  constructor(private auth: AuthService) { }

  canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
    // TODO: remove this line in production
    return true;

    // console.log(this.auth.isLoggedIn() && this.auth.isAdmin(), ' redirecting from admin module loading');
    // return this.auth.isLoggedIn() && this.auth.isAdmin();
  }
}
