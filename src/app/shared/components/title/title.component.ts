import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.css']
})
export class TitleComponent {
  _title: string;

  @Input()
  set title(title: string) { this._title = title; }
  get title(): string { return this._title; }
}
