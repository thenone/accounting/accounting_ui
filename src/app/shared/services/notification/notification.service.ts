import { Injectable } from '@angular/core';

// NotifyJs jQuery selector
declare let $: any;

@Injectable()
export class NotificationService {
  success(...messages: string[]) {
    $.notify(messages.join(' - '), {
      className: 'success'
    });
  }

  error(...messages: string[]) {
    $.notify(messages.join(' - '), {
      className: 'error'
    });
  }
}
