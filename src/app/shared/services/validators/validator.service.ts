import { Injectable } from '@angular/core';
import { AbstractControl, FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';

import { Customer } from '../../models/customer';
import {Loan} from '../../models/loan';

@Injectable()
export class ValidatorService {
  constructor() { }

  static mobile(control: AbstractControl): ValidationErrors {
    const value = control.value;
    if (value && value.length < 2 || isNaN(Number(value))) {
      control.setValue('09');
    }
    if (value.length !== 11) {
      return {length: true};
    }
    if (value.substr(0, 2) !== '09') {
      return {invalid: true};
    }
    return null;
  }

  static passwordMatch(group: FormGroup): ValidationErrors | null {
    if (group.controls.first.value !== group.controls.confirm.value) {
      group.controls.confirm.setErrors({passwordMatch: true});
      group.setErrors({passwordMatch: true});
      return {passMatch: true};
    }
    group.controls.confirm.setErrors(null);
    return null;
  }

  static nationalId(control: AbstractControl): ValidationErrors {
    if (
      (control.value.charAt(0) === '0' && control.value.length !== 10) ||
      (control.value.charAt(0) !== '0' && control.value.length !== 9)
    ) {
      return {nationalId: 'نامعتبر'};
    }
  }

  static text(control: AbstractControl): ValidationErrors {
    if (control.value.length > 0 && /\d/.test(control.value)) {
      return {text: true};
    }
  }

  static number(control: AbstractControl): ValidationErrors {
    if (isNaN(control.value)) {
      return {number: true};
    }
  }

  static customerChoice(customersId: Customer[]): ValidatorFn {
    return (control: AbstractControl) => {
      if (customersId && customersId.length > 0 && customersId.indexOf(control.value) === -1) {
        return {customer: true};
      }
    };
  }

  static loanChoiceOnActivate(loans: Loan[]): ValidatorFn {
    return (group: FormGroup) => {
      if (loans && loans.length > 0 && group.controls.isActive && loans.indexOf(group.controls.loanId.value) === -1) {
        return {loanId: true};
      }
    };
  }
}
