import { TestBed, inject } from '@angular/core/testing';

import { PersianDateService } from './persian-date.service';

describe('PersianDateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PersianDateService]
    });
  });

  it('should be created', inject([PersianDateService], (service: PersianDateService) => {
    expect(service).toBeTruthy();
  }));
});
