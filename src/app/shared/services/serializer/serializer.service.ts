import { Injectable } from '@angular/core';

import * as $ from 'jquery';

import { PersianDateService } from '../date/persian-date.service';

@Injectable()
export class SerializerService {

  constructor(private persianDate: PersianDateService) { }

  serialize(object) {
    const serialized = object;
    if (serialized.username && serialized.username.charAt(1) === '9') {
      serialized.username = serialized.username.substr(1);
    }
    if (serialized.mobile && serialized.mobile.charAt(1) === '9') {
      serialized.mobile = serialized.mobile.substr(1);
    }
    if (serialized.join_date) {
      serialized.join_date = this.persianDate.ISODate(serialized.join_date, '-');
    }
    if (serialized.date) {
      serialized.date = this.convertDate(serialized.date);
    }
    return $.param(serialized);
  }

  private convertDate(date: string) {
    return this.persianDate.ISODate(date, '-');
  }
}
