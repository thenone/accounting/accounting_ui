import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

import { LoansService } from '../../services/loans/loans.service';
import { PaymentsService } from '../../services/payments/payments.service';
import { CustomersService } from '../../services/customers/customers.service';
import { NotificationService } from '../../../shared/services/notification/notification.service';

import { Loan } from '../../../shared/models/loan';
import { Payment } from '../../../shared/models/payment';
import { Customer } from '../../../shared/models/customer';

declare var $: any;

@Component({
  selector: 'app-payment-add',
  templateUrl: './payment-add.component.html',
  styleUrls: ['./payment-add.component.css']
})
export class PaymentAddComponent implements OnInit {

  constructor(
    private loansService: LoansService,
    private paymentsService: PaymentsService,
    private customersService: CustomersService,
    private notify: NotificationService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  payment: Payment;
  customers: Customer[];
  loan: Loan;
  customerId: string;
  isLoanLoaded = false;
  normalInstallmentAmount = 0;
  finalInstallmentAmount = 0;

  errors: { [key: string]: any; } = { };

  finalPaymentDisabled = false;
  normalPaymentDisabled = false;

  noUnpaidLoanError: string;

  ngOnInit() {
    this.payment = new Payment();
    $('.ui.radio.checkbox')
      .checkbox()
    ;
    this.customersService.getIds({ serialize_group: 'dropdown' }).subscribe(
      customers => {
        this.customers = customers as Customer[];
      },
      error => {
        this.notify.error('خطا در دریافت لیست مشتریان', 'دوباره امتحان کنید');
      }
    );
  }

  onTotalBlur() {
    if (!this.noUnpaidLoanError && this.payment.total < this.loan.payments[this.payment.installment_type]['amount']) {
      this.errors = { total: 'مبلغ از قسط کمتر است' };
      return;
    } else {
      this.errors = { };
    }
  }

  onCustomerChange(event) {
    this.resetForm();
    this.customerId = event.target.value;
    this.loansService.getList({ customer: event.target.value, unpaid: true }).subscribe(
      loan => {
        if (Array.isArray(loan) && loan.length === 0) {
          this.noUnpaidLoanError = 'وام مشتری به طور کامل پرداخت شده است. واریزی به عضویت مشتری اضافه می‌شود.';
        } else {
          this.onGetLoan(loan as Loan);
        }
      },
      error => {
        this.notify.error('خطا در دریافت وام', 'اتصال اینترنت را بررسی کنید');
      }
  );
  }

  onGetLoan(loan: Loan) {
    this.loan = loan;
    this.normalInstallmentAmount = this.loan.payments.normal.amount;
    this.finalInstallmentAmount = this.loan.payments.final.amount;
    if (this.loan.payments.final.paid) {
      this.finalPaymentDisabled = true;
      this.payment.installment_type = 'normal';
    }
    if (this.loan.payments.normal.paidQty === this.loan.payments.normal.qty) {
      this.normalPaymentDisabled = true;
      if (this.loan.payments.final.paid) {
        this.payment.installment_type = '';
        return;
      } else {
        this.payment.installment_type = 'final';
      }
    }
    this.isLoanLoaded = true;
  }

  resetForm() {
    this.noUnpaidLoanError = '';
    this.isLoanLoaded = false;
    this.finalInstallmentAmount = 0;
    this.normalInstallmentAmount = 0;
    this.finalPaymentDisabled = false;
    this.normalPaymentDisabled = false;
    this.payment = new Payment();
  }

  public onSubmit() {
    if (!this.payment.installment_type || !!this.noUnpaidLoanError) {
      const membershipFee = { total: this.payment.total, customer_id: this.customerId, date: this.payment.date };
      this.paymentsService.addToMembershipFee(membershipFee).subscribe(
        res => this.onSuccess(res as Payment),
        err => this.onError(err)
      );
      return;
    }
    this.payment.loan = this.loan._id;
    console.log(this.payment);
    this.paymentsService.register(this.payment).subscribe(
      res => this.onSuccess(res as Payment),
      error => this.onError(error)
    );
  }

  private onSuccess(res: Payment) {
    this.notify.success('پرداخت با موفقیت ثبت شد');
    this.router.navigate(['../payments'], { relativeTo: this.activatedRoute });
  }

  private onError(error: HttpErrorResponse) {
    if (error.status === 400) {
      this.notify.error('خطا', error.error.message);
    }
  }

}
